package com.sample

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class GeneraltestApplication {

	static void main(String[] args) {
		SpringApplication.run GeneraltestApplication, args
	}
}
