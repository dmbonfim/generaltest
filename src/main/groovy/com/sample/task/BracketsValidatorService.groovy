package com.sample.task

import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

@Service
class BracketsValidatorService {

    private Map <String, String> map = ['(':')', '[':']', '{':'}']
    private List<String> closingBrackets

    @PostConstruct
    void init() {
        closingBrackets = map.values() as List
    }

    public boolean checkBalance(String expression) {
        List stack = []
        for (String c in expression) {
            if(map[c]) {
                stack.push(map[c])
            }
            if(c in closingBrackets){
                if (stack.isEmpty())  { return false }
                if (stack.pop() != c) { return false }
            }
        }
        return stack.isEmpty()
    }
}
