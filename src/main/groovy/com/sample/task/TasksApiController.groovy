package com.sample.task

import io.swagger.model.BalanceTestResult
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

import javax.validation.constraints.NotNull

@RestController
@ControllerAdvice
class TasksApiController {

    @Autowired
    BracketsValidatorService bracketsValidatorService

    @GetMapping(value   = "/tasks/validateBrackets",
                produces = [ "application/json" ])
    @ResponseStatus(HttpStatus.OK)
    BalanceTestResult validateBrackets(@NotNull @RequestParam String input) {
        return new BalanceTestResult(
                input: input,
                isBalanced: bracketsValidatorService.checkBalance(input)
        )
    }

}
