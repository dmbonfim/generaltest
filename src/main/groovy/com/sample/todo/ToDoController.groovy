package com.sample.todo

import com.sample.todo.model.ToDoItemAddRequest
import com.sample.todo.model.ToDoItem
import io.swagger.model.ToDoItemNotFoundError
import io.swagger.model.ToDoItemNotFoundErrorDetails
import io.swagger.model.ToDoItemUpdateRequest
import io.swagger.model.ToDoItemValidationError
import io.swagger.model.ToDoItemValidationErrorDetails
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException

import javax.validation.Valid
import javax.validation.constraints.NotNull

@RestController
@ControllerAdvice
class ToDoController {

    @Autowired
    ToDoService toDoService

    @PostMapping(value    = "/todo",
            consumes = [ "application/json" ],
            produces = [ "application/json" ])
    @ResponseStatus(HttpStatus.OK)
    ToDoItem createItem(@Valid @RequestBody ToDoItemAddRequest command) {
        return toDoService.create(command.text)
    }

    @GetMapping(value    = "/todo/{id}", produces = [ "application/json" ])
    @ResponseStatus(HttpStatus.OK)
    ToDoItem retrieveItem(@NotNull @Valid @PathVariable("id") Long id) {
        return toDoService.retrieve(id)
    }

    @PatchMapping(value = "/todo/{id}", produces = [ "application/json" ])
    @ResponseStatus(HttpStatus.OK)
    ToDoItem updateItem(@NotNull @Valid @PathVariable("id") Long id,
                        @Valid @RequestBody ToDoItemUpdateRequest updateRequest ) {
        return toDoService.update(id, updateRequest)
    }

    @ExceptionHandler(MethodArgumentNotValidException)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ToDoItemValidationError handleValidationError(MethodArgumentNotValidException e) {
        return new ToDoItemValidationError(
                name: "ValidationError",
                details: e.bindingResult.fieldErrors.collect {
                    return new ToDoItemValidationErrorDetails(
                            location: "params",
                            param: it.field,
                            msg: it.defaultMessage,
                            value: it.rejectedValue
                    )
                }
        )
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ToDoItemValidationError handleValidationError(MethodArgumentTypeMismatchException e) {
        return new ToDoItemValidationError(
                name: "ValidationError",
                details: [
                        new ToDoItemValidationErrorDetails(
                                location: "params",
                                param: e.name,
                                msg: "Parameter should be a number",
                                value: e.value
                        )
                ]
        )
    }

    @ExceptionHandler(HttpMessageNotReadableException)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ToDoItemValidationError handleValidationError(HttpMessageNotReadableException e) {
        return new ToDoItemValidationError(
                name: "ValidationError",
                details: [
                        new ToDoItemValidationErrorDetails(
                                location: "params",
                                param: "",
                                msg: e.message,
                                value: "Not Readable"
                        )
                ]
        )
    }

    @ExceptionHandler(ToDoItemNotFoundException)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ToDoItemNotFoundError handleItemNoFound(ToDoItemNotFoundException e) {
        return new ToDoItemNotFoundError(
                name: "NotFoundError",
                details: [
                        new ToDoItemNotFoundErrorDetails(message: e.message)
                ]
        )
    }

}
