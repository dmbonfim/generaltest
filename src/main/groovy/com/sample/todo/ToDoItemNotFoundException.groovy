package com.sample.todo

import groovy.transform.InheritConstructors

@InheritConstructors
class ToDoItemNotFoundException extends RuntimeException{
}
