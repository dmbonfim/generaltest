package com.sample.todo

import com.sample.todo.model.ToDoItem
import org.springframework.data.jpa.repository.JpaRepository

import javax.transaction.Transactional

@Transactional
interface ToDoRepository extends JpaRepository<ToDoItem, Integer> { }
