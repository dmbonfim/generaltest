package com.sample.todo

import com.sample.todo.model.ToDoItem
import io.swagger.model.ToDoItemUpdateRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.transaction.Transactional
import java.time.Clock
import java.time.OffsetDateTime

@Service
class ToDoService {

    @Autowired
    ToDoRepository repository

    @Autowired
    Clock clock

    @Transactional
    ToDoItem create(String text) {
        ToDoItem item = new ToDoItem(
                text: text,
                isCompleted: false,
                createdAt: OffsetDateTime.now(clock)
        )
        return repository.save(item)
    }

    @Transactional
    ToDoItem retrieve(Long id) {
        Optional<ToDoItem> toDoItem = repository.findById(id)
        if(!toDoItem.isPresent()) {
            throw new ToDoItemNotFoundException("Item with id ${id} not found")
        }
        return toDoItem.get()
    }

    @Transactional
    ToDoItem update(Long id, ToDoItemUpdateRequest updateRequest) {
        ToDoItem item = retrieve(id)
        item.with {
            text = updateRequest.text ?: item.text
            isCompleted = updateRequest.isCompleted ?: item.isCompleted
        }
        return repository.save(item)
    }
}
