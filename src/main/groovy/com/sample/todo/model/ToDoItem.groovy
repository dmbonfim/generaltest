package com.sample.todo.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import java.time.OffsetDateTime
import javax.persistence.Entity

@Entity
@EqualsAndHashCode
@ToString(includeFields = true)
class ToDoItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    String text

    Boolean isCompleted

    OffsetDateTime createdAt
}
