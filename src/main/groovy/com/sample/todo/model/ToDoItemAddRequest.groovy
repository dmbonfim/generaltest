package com.sample.todo.model

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ToDoItemAddRequest {
    @NotNull(message = "Input must be provided")
    @Size(min=1, max=50, message = "size must be between 1 and 50")
    String text
}
