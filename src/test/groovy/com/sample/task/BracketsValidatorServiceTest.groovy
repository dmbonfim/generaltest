package com.sample.task

import spock.lang.Specification
import spock.lang.Unroll

class BracketsValidatorServiceTest extends Specification {

    BracketsValidatorService bracketsValidatorService
    Map<String, String> bracketsMap

    void setup() {
        bracketsMap = ['(':')', '[':']', '{':'}']
        bracketsValidatorService = new BracketsValidatorService(map: bracketsMap)
        bracketsValidatorService.init()
    }

    @Unroll
    def "test checkBalance"() {
        when:
        boolean result = bracketsValidatorService.checkBalance(expression)

        then:
        result == expectedResult
        noExceptionThrown()

        where:
        expression       | expectedResult
        "{a+b[x]}"       | true
        "({a+b[x]})"     | true
        "()"             | true
        ""               | true
        "a+b"            | true
        "{a+b{x]}"       | false
        "{a+b[x]}}"      | false
        "({a+b[x]}"      | false
        "["              | false

    }
}

