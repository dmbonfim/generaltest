package com.sample.task

import com.fasterxml.jackson.databind.ObjectMapper
import io.swagger.model.BalanceTestResult
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class TasksApiControllerTest extends Specification {

    TasksApiController tasksApiController

    MockMvc mockMvc
    ObjectMapper mapper
    BracketsValidatorService bracketsValidatorService

    def setup() {
        bracketsValidatorService = Mock(BracketsValidatorService)
        tasksApiController = new TasksApiController(
                bracketsValidatorService: bracketsValidatorService
        )
        mapper = new ObjectMapper()
        mockMvc = MockMvcBuilders.standaloneSetup(tasksApiController).build()
    }

    void "GET /tasks/validateBrackets with valid input should return HTTP 200"() {
        given:
        String input = "[a+b]"

        expect:
        mockMvc.perform(get("/tasks/validateBrackets")
                .param("input", input))
                .andExpect(status().isOk())
    }

    void "GET /tasks/validateBrackets with valid input should return validation result"() {
        given:
        String input = "[a+b]"
        boolean expectedValidationResult = true

        when:
        def result = mockMvc.perform(get("/tasks/validateBrackets")
                .param("input", input))
                .andExpect(status().isOk())
                .andReturn()
        String bodyContent = result.getResponse().getContentAsString()
        BalanceTestResult balanceTestResult = mapper.readValue(bodyContent, BalanceTestResult)

        then:
        1 * bracketsValidatorService.checkBalance(input) >> expectedValidationResult
        0 * _
        balanceTestResult
        balanceTestResult.input == input
        balanceTestResult.isBalanced == expectedValidationResult
    }

}