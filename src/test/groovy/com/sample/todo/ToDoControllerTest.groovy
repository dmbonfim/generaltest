package com.sample.todo

import com.fasterxml.jackson.databind.ObjectMapper
import com.sample.todo.model.ToDoItemAddRequest
import com.sample.todo.model.ToDoItem
import io.swagger.model.ToDoItemNotFoundError
import io.swagger.model.ToDoItemUpdateRequest
import io.swagger.model.ToDoItemValidationError
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.*

import java.time.Clock
import java.time.OffsetDateTime

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class ToDoControllerTest extends Specification {

    ToDoController toDoController

    ToDoService toDoService

    MockMvc mockMvc

    ObjectMapper mapper

    @Shared
    String lengthValidationMessage = "size must be between 1 and 50"

    @Shared
    String nullValidationMessage = "Input must be provided"

    def setup() {
        toDoService = Mock(ToDoService)
        toDoController = new ToDoController(
                toDoService: toDoService
        )
        mapper = new ObjectMapper()
        mapper.findAndRegisterModules()
        mockMvc = MockMvcBuilders.standaloneSetup(toDoController).build()
    }

    void "POST /todo with valid input should return HTTP 200"() {
        given:
        ToDoItemAddRequest command = new ToDoItemAddRequest(
                text: "New TODO sample task"
        )

        expect:
        mockMvc.perform(post("/todo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(command)))
                .andExpect(status().isOk())
    }

    void "POST /todo with valid input should return the generated entity"() {
        given:
        ToDoItemAddRequest command = new ToDoItemAddRequest(
                text: "New TODO sample task"
        )
        ToDoItem expectedCreatedItem = new ToDoItem(
                id: 1,
                text: command.text,
                isCompleted: false,
                createdAt: OffsetDateTime.now(Clock.systemUTC())
        )

        when:
        def result = mockMvc.perform(post("/todo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(command)))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItem toDoItem = mapper.readValue(bodyContent, ToDoItem)

        then:
        1 * toDoService.create(command.text) >> expectedCreatedItem
        toDoItem == expectedCreatedItem
    }

    @Unroll
    void "POST /todo with invalid input should return HTTP 400 with custom error message"() {
        given:
        ToDoItemAddRequest command = new ToDoItemAddRequest(
                text: text
        )

        when:
        def result = mockMvc.perform(post("/todo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(command)))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItemValidationError error = mapper.readValue(bodyContent, ToDoItemValidationError)

        then:
        result.andExpect(status().isBadRequest())
        error.details.size() == 1
        error.details.first().msg == expectedMessage

        where:
        text                         | expectedMessage
        null                         | nullValidationMessage
        ""                           | lengthValidationMessage
        RandomStringUtils.random(51) | lengthValidationMessage
    }

    void "GET /todo/{id} with valid input should return HTTP 200"() {
        given:
        Long id = 10

        expect:
        mockMvc.perform(get("/todo/${id}"))
                .andExpect(status().isOk())
    }

    void "GET /todo/{id} with existent id should return HTTP 200 and the item retrieved"() {
        given:
        Long id = 5
        ToDoItem sampleSavedItem = new ToDoItem(
                id: id,
                text: "sample text",
                isCompleted: false,
                createdAt: OffsetDateTime.now(Clock.systemUTC())
        )
        when:
        def result = mockMvc.perform(get("/todo/${id}"))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItem item = mapper.readValue(bodyContent, ToDoItem)

        then:
        1 * toDoService.retrieve(id) >> sampleSavedItem
        result.andExpect(status().isOk())
        item == sampleSavedItem
    }

    void "GET /todo/{id} should not fail to parse long ids"() {
        given:
        String id = "9007199254740991"
        Long longId = Long.parseLong(id)
        ToDoItem sampleSavedItem = new ToDoItem(
                id: longId,
                text: "sample text",
                isCompleted: false,
                createdAt: OffsetDateTime.now(Clock.systemUTC())
        )
        when:
        def result = mockMvc.perform(get("/todo/${id}"))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItem item = mapper.readValue(bodyContent, ToDoItem)

        then:
        1 * toDoService.retrieve(longId) >> sampleSavedItem
        result.andExpect(status().isOk())
        item == sampleSavedItem
    }

    void "GET /todo/{id} with invalid input should return HTTP 400 with custom error message"() {
        when:
        def result = mockMvc.perform(get("/todo/blah"))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItemValidationError error = mapper.readValue(bodyContent, ToDoItemValidationError)

        then:
        result.andExpect(status().isBadRequest())
        error.details.size() == 1
        error.details.first().msg == "Parameter should be a number"
    }

    void "GET /todo/{id} with non-existent id should return HTTP 404 with custom error message"() {
        given:
        Long nonExistentId = 5000
        String expectedErrorMessage = "Item not found"

        when:
        def result = mockMvc.perform(get("/todo/${nonExistentId}"))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItemNotFoundError error = mapper.readValue(bodyContent, ToDoItemNotFoundError)

        then:
        1 * toDoService.retrieve(nonExistentId) >> { throw new ToDoItemNotFoundException(expectedErrorMessage) }
        result.andExpect(status().isNotFound())
        error.details.size() == 1
        error.details.first().message == expectedErrorMessage
    }

    void "PATCH /todo/{id} with existent id should return HTTP 200 and the item updated"() {
        given:
        Long id = 5
        ToDoItemUpdateRequest updateRequest = new ToDoItemUpdateRequest(
                text: "updated text",
                isCompleted: true,
        )
        ToDoItem sampleUpdatedItem = new ToDoItem(
                id: id,
                text: updateRequest.text,
                isCompleted: updateRequest.isCompleted,
                createdAt: OffsetDateTime.now(Clock.systemUTC())
        )
        when:
        def result = mockMvc.perform(patch("/todo/${id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateRequest)))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItem item = mapper.readValue(bodyContent, ToDoItem)

        then:
        1 * toDoService.update(id, updateRequest) >> sampleUpdatedItem
        result.andExpect(status().isOk())
        item == sampleUpdatedItem
    }

    void "PATCH /todo/{id} with non-existent id should return HTTP 404 with custom error message"() {
        given:
        Long nonExistentId = 5000
        ToDoItemUpdateRequest updateRequest = new ToDoItemUpdateRequest(
                text: "updated text",
                isCompleted: true,
        )
        String expectedErrorMessage = "Item not found"

        when:
        def result = mockMvc.perform(patch("/todo/${nonExistentId}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateRequest)))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItemNotFoundError error = mapper.readValue(bodyContent, ToDoItemNotFoundError)

        then:
        1 * toDoService.update(nonExistentId, updateRequest) >> {
            throw new ToDoItemNotFoundException(expectedErrorMessage)
        }
        0 * _
        result.andExpect(status().isNotFound())
        error.details.size() == 1
        error.details.first().message == expectedErrorMessage
    }

    void "PATCH /todo/{id} with invalid id should return HTTP 400 with custom error message"() {
        given:
        ToDoItemUpdateRequest updateRequest = new ToDoItemUpdateRequest(
                text: "updated text",
                isCompleted: true,
        )

        when:
        def result = mockMvc.perform(patch("/todo/blah")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateRequest)))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItemValidationError error = mapper.readValue(bodyContent, ToDoItemValidationError)

        then:
        result.andExpect(status().isBadRequest())
        error.details.size() == 1
        error.details.first().msg == "Parameter should be a number"
    }

    void "PATCH /todo/{id} with long text should return HTTP 400 with custom error message"() {
        given:
        Long id = 5
        ToDoItemUpdateRequest updateRequest = new ToDoItemUpdateRequest(
                text: RandomStringUtils.random(51),
                isCompleted: true,
        )

        when:
        def result = mockMvc.perform(patch("/todo/${id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(updateRequest)))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItemValidationError error = mapper.readValue(bodyContent, ToDoItemValidationError)

        then:
        result.andExpect(status().isBadRequest())
        error.details.size() == 1
        error.details.first().msg == lengthValidationMessage
    }

    void "PATCH /todo/{id} with empty update request return HTTP 400 with custom error message"() {
        when:
        def result = mockMvc.perform(patch("/todo/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
        String bodyContent = result.andReturn().getResponse().getContentAsString()
        ToDoItemValidationError error = mapper.readValue(bodyContent, ToDoItemValidationError)

        then:
        result.andExpect(status().isBadRequest())
        error.details.size() == 1
        error.details.first().value == "Not Readable"
    }
}