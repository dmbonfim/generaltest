package com.sample.todo

import com.sample.todo.model.ToDoItem
import io.swagger.model.ToDoItemUpdateRequest
import spock.lang.*

import java.time.Clock
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneOffset

class ToDoServiceTest extends Specification {

    ToDoRepository repository
    Clock clock
    ToDoService toDoService

    def setup() {
        repository = Mock(ToDoRepository)
        clock = Clock.fixed(Instant.now(), ZoneOffset.ofHours(10))
        toDoService = new ToDoService(
                clock: clock,
                repository: repository
        )
    }

    def "test create should create and save a new ToDoItem with default attributes"() {
        given:
        String sampleText = "Do the test"
        OffsetDateTime now = OffsetDateTime.now(clock)
        ToDoItem itemToBeSaved = new ToDoItem(
                id: null,
                text: sampleText,
                isCompleted: false,
                createdAt: now
        )
        ToDoItem expectedCreatedItem = new ToDoItem(
                id: 1,
                text: sampleText,
                isCompleted: false,
                createdAt: now
        )

        when:
        ToDoItem result = toDoService.create(sampleText)

        then:
        1 * repository.save(itemToBeSaved) >> expectedCreatedItem
        result == expectedCreatedItem
    }

    def "test retrieve should return the item given the item id"() {
        given:
        Integer id = 1
        ToDoItem expectedSavedItem = new ToDoItem(
                id: 1,
                text: "sample text",
                isCompleted: false,
                createdAt: OffsetDateTime.now()
        )

        when:
        ToDoItem result = toDoService.retrieve(id)

        then:
        1 * repository.findById(id) >> Optional.of(expectedSavedItem)
        result == expectedSavedItem
    }

    def "test retrieve with non-existent item should throw exception"() {
        given:
        Integer id = 1000

        when:
        toDoService.retrieve(id)

        then:
        1 * repository.findById(id) >> Optional.empty()
        thrown(ToDoItemNotFoundException)
    }

    def "test update should update and return the given item if id exists"() {
        given:
        Integer id = 1
        ToDoItemUpdateRequest updateRequest = new ToDoItemUpdateRequest(
                text: "updated sample text",
                isCompleted: false,
        )
        ToDoItem currentSavedItem = new ToDoItem(
                id: id,
                text: "current text",
                isCompleted: false,
                createdAt: OffsetDateTime.now(Clock.systemUTC())
        )
        ToDoItem expectedUpdatedItem = new ToDoItem(
                id: id,
                text: updateRequest.text,
                isCompleted: updateRequest.isCompleted,
                createdAt: currentSavedItem.createdAt
        )

        when:
        ToDoItem result = toDoService.update(id, updateRequest)

        then:
        1 * repository.findById(id) >> Optional.of(currentSavedItem)
        1 * repository.save(expectedUpdatedItem) >> expectedUpdatedItem
        result == expectedUpdatedItem
    }

    def "test update should only change the given parameter"() {
        given:
        Integer id = 1
        ToDoItemUpdateRequest textOnlyUpdateRequest = new ToDoItemUpdateRequest(
                text: "updated sample text",
        )
        ToDoItem currentSavedItem = new ToDoItem(
                id: id,
                text: "current text",
                isCompleted: false,
                createdAt: OffsetDateTime.now(Clock.systemUTC())
        )
        ToDoItem expectedUpdatedItem = new ToDoItem(
                id: currentSavedItem.id,
                text: textOnlyUpdateRequest.text,
                isCompleted: currentSavedItem.isCompleted,
                createdAt: currentSavedItem.createdAt
        )

        when:
        ToDoItem result = toDoService.update(id, textOnlyUpdateRequest)

        then:
        1 * repository.findById(id) >> Optional.of(currentSavedItem)
        1 * repository.save(expectedUpdatedItem) >> expectedUpdatedItem
        result == expectedUpdatedItem
    }

    def "test update should throw exception if the given item id does not exist"() {
        given:
        Integer nonExistentId = 1
        ToDoItemUpdateRequest updatedRequest = new ToDoItemUpdateRequest(
                text: "new updated request",
                isCompleted: true,
        )

        when:
        toDoService.update(nonExistentId, updatedRequest)

        then:
        1 * repository.findById(nonExistentId) >> Optional.empty()
        0 * _
        thrown(ToDoItemNotFoundException)
    }
}